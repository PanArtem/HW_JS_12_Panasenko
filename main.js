const gallery = $('img');
let index = 0;

const showImg = () => {
    gallery.each((i, el) => {
       $(el).css('display', 'none');
    });
    gallery[index].style.display = 'block';
    // $(gallery(index)).css('display', 'block'); - почему не работает?
    index++;
    if (index === gallery.length) {
        index = 0
    }
};
showImg();

let interval = setInterval(showImg, 1000);

const stopBtn = document.createElement('span');
$(stopBtn).appendTo('body')
          .text('Прекратить')
          .css('backgroundColor', 'red');
$(stopBtn).on('click', () => {
    clearInterval(interval);
});

const startBtn = document.createElement('span');
$(startBtn).appendTo('body')
           .text('Возобновить')
           .css('backgroundColor', 'green');
$(startBtn).on('click', () => {
    interval = setInterval(showImg, 1000);
});


/*
$('.img').each((i,elem) => {
    $(elem).fadeOut(0);
    $(elem).delay(i * 2000)
        .fadeIn(2000)
        .fadeOut(0)
});
*/
